# -*- coding: utf-8 -*-

class Calculo(object):
    """docstring for Calculo"""
    def __init__(self, arg):
        super(Calculo, self).__init__()
        self.arg = arg

    def juros_anual_para_mensal(juros_anual, taf=0):
            """
            conversor de juros anual em mensal
            """

            return (((1 + juros_anual) / (1 + taf)) ** (1.0 / 12.0)) - 1
