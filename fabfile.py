# -*- coding: utf-8 -*-
"""
Deployment automation Fabric script for titans-contract-manager
"""

from fabric.api import cd, env, run, sudo, prefix
from fabric.context_managers import settings
from fabric.contrib.files import append, contains, exists

# Default settings
env.forward_agent = True
env.name = None
env.new_relic = True
env.repository = 'git@@bitbucket.org:rfukui/novo_hidra.git'
env.user = 'ubuntu'
env.user_home = '/home/%s' % env.user
env.src_path = '%s/novo_hidra' % env.user_home
env.venv_path = '%s/.venv' % env.src_path


def localhost():
    """
    Set the dev settings
    """
    env.name = 'dev'
    env.new_relic = True
    env.hosts = [
        '127.0.0.1',
    ]
    env.roledefs = {"single": [env.hosts[0]],
                    "cron": [env.hosts[0]],
                    "web": [env.hosts[0]]}


def deploy(commit='master'):
    """
    Deploy the app in the server
    """
    if not env.name:
        raise Exception(u'You MUST set the environment variable.')

    # SSH exclude key checking for github.com
    if not exists('%s/.ssh' % env.user_home):
        run('mkdir %s/.ssh' % env.user_home)

    ssh_config = '%s/.ssh/config' % env.user_home
    if not contains(ssh_config, 'Host github.com'):
        run('echo "Host bitbucket.org" >>%s' % ssh_config)
        run('echo "     StrictHostKeyChecking no" >>%s' % ssh_config)
        run('echo "     UserKnownHostsFile /dev/null" >>%s' % ssh_config)

    # clone the repo into the env.src_path
    if not exists(env.src_path):
        run('git clone %s' % env.repository)

    with cd(env.src_path):
        # fetch the changes
        run('git fetch')

        # checkout to the selected commit/tag/branch
        run('git checkout %s' % commit)

        # if the selected commit is the master branch, merge the changes
        with settings(warn_only=True):
            is_branch = run('git branch -r | grep \'%s\'' % commit).succeeded

            # if the selected commit is the master branch, merge the changes
            if is_branch:
                run('git merge origin/%s' % commit)

        # create the deployment specific folders
        if not exists('logs') or not exists('pid') or not exists('sock') or not exists('bin'):
            run('mkdir -p logs pid sock bin')

        # create virtualenv if needed
        if not exists(env.venv_path):
            python = '/usr/bin/python'
            run('virtualenv -p %(python)s %(path)s' % {'path': env.venv_path, 'python': python})

        with prefix('source %s/bin/activate' % env.venv_path):
            # install requirements
            run('pip install -r requirements.txt')
            #run migrations
            management('migrate')

    if env.name == 'production':
        notify_deploy(commit)

    print "WARNING: deploy do not restart the service"


def setup_python():
    if not contains('/etc/apt/sources.list', 'deb http://mirror.cse.iitk.ac.in/debian/ testing main contrib', use_sudo=True):
        append('/etc/apt/sources.list', 'deb http://mirror.cse.iitk.ac.in/debian/ testing main contrib', use_sudo=True)
        sudo('aptitude update')
        sudo('aptitude install python2.7')



def setup_app_server():
    """
    Setup server applications
    """
    # Update the server packages
    # sudo('apt-get update && apt-get upgrade -y')

    # Install build dependencies
    sudo('apt-get install -y --force-yes build-essential git libmysqlclient-dev libxml2-dev libxslt1-dev libmemcached-dev mercurial')

    # Python distribute, pip, virtualenv, python headers and needed libs for requirements
    sudo('apt-get install -y --force-yes python-distribute python-pip python-dev libjpeg8-dev libfreetype6-dev liblcms1-dev gettext')
    sudo('pip install virtualenv')
    # NTP
    sudo('apt-get install ntp -y')
    # Python distribute, pip, virtualenv, python headers and needed libs for requirements
    sudo('apt-get install -y python-distribute python-pip python-dev libjpeg8-dev libfreetype6-dev liblcms1-dev gettext')
    sudo('pip install virtualenv')
    # uWSGI
    sudo('pip install uwsgi==1.4.4')  # install to /usr/local/bin/uwsgi

    # NTP
    sudo('apt-get install ntp -y')

    # curl, needed for new-relic notification
    sudo('apt-get install curl -y')


def foo():
    print "bar"