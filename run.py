import simplejson as json
import datetime
from model import Pessoa
import xlrd
from flask import Flask, render_template, jsonify, request
from werkzeug.utils import secure_filename
ALLOWED_EXTENSIONS = set(['xls'])
app = Flask(__name__)


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS



@app.route('/html/', methods = ["GET", "POST"])
def html():
    return render_template('index.html')


@app.route('/upload', methods = ["POST"])
def upload():
    _dict = {
     "id":'Matric',
     "nome":'Nome',
     "data_nascimento":'Nacto',
     "sexo":'Sx Ativo',
     "idade_aposentadoria":'Id Apos',
     "contribuicao_mensal":'Ctr Part',
     "contribuicao_empresa":'Ctr Empr',
     "aporte_inicial":'Aport Part',
     "aporte_empresa":'Aport Emp',  
    }
    
    for k in _dict.keys():
        if request.form[k].strip() != "":
            _dict[k] = request.form[k]
    if not request.files['file']:
        return "inserir arquivo", 500
    
    xlr = request.files['file']
    pessoas = []
    print datetime.datetime.now()
    if xlr and allowed_file(xlr.filename):
        try:
            book = xlrd.open_workbook(file_contents = xlr.read())
        except xlrd.XLRDError, e:
            return "arquivo corrompido ou invalido", 500
        worksheet = book.sheet_by_index(0)
        cells = []
        for cell in worksheet.row(0):
            cells.append(cell.value)

        indice={}

        for k in _dict:
            try:
                indice[k] = cells.index(_dict[k])
            except ValueError, e:
                return "valor invalido para chave de: " + k + ", verifique se na planilha o valor da celula equivalente ao mesmo eh: " + _dict[k], 500
            

        
        for x in range(1, worksheet.nrows-1):
            pessoa = {}
            for k,v in indice.items():
                cell = worksheet.cell_value(x, v)
                # Cell Types: 0=Empty, 1=Text, 2=Number, 3=Date, 4=Boolean, 5=Error, 6=Blank
                if worksheet.cell_type(x, v) == 3: 
                    cell = datetime.datetime(*xlrd.xldate_as_tuple(cell, book.datemode))
                pessoa[k] = cell
            
            pessoas.append(Pessoa(pessoa))
   
        print datetime.datetime.now()
        return render_template("pessoas.html", pessoas = pessoas)
    else:
        return "arquivo invalido", 500




@app.route('/mackenzie/', methods = ["GET", "POST"])
#@app.route('/mackenzie', methods = ["GET", "POST"])
def mackenzie():
    if request.data:
        req = json.loads(request.data)
        req["salario"] = float(req["salario"])
        valor_ur = {"professor":  335.42 , "administrativo" : 346.16}
    
        if (req["salario"]/valor_ur[req["cargo"]]) < 14.0:
            #print "2%"
            req["contribuicao_empresa"] = req["salario"] * 0.01
            req["contribuicao_mensal"] = req["salario"] * 0.02
            req["percentual_salario"] = "2%"
        elif (req["salario"]/valor_ur[req["cargo"]]) >= 14.0 and (req["salario"]/valor_ur[req["cargo"]]) < 33.0:
            #print "4%"
            req["contribuicao_empresa"] = req["salario"] * 0.02
            req["contribuicao_mensal"] = req["salario"] * 0.04
            req["percentual_salario"] = "4%"
        else:
            #print "5%"
            req["contribuicao_empresa"] = req["salario"] * 0.025
            req["contribuicao_mensal"] = req["salario"] * 0.05
            req["percentual_salario"] = "5%"

        #print req    
        pessoa = Pessoa(req)
        pessoa.calcula()


        return jsonify(id = pessoa.id, nome = pessoa.nome, montante_pessoa = pessoa.montante,
            montante_empresa = pessoa.montante_empresa, contrib_empresa = pessoa.contribuicao_empresa, contrib_pessoa = pessoa.contribuicao_mensal, 
            fator_atuarial = pessoa.fator_atuarial(), percentual_salario = req["percentual_salario"], total_reserva = pessoa.montante_empresa +  pessoa.montante)
     #return jsonify(json.loads(request.data))
    else:
        return jsonify( nome = "mack sem json", montante_pessoa = 100.00, montante_empresa = 50.00, contrib_empresa = 25.00, contrib_pessoa = 50.00, percentual_salario ="10%")


@app.route('/', methods = ["GET", "POST"])
def hello_world():
    #print request.data
    if request.data:
        req = json.loads(request.data)
        pessoa = Pessoa(req)
        pessoa.calcula()

        return jsonify(id = pessoa.id, nome = "com json", montante_pessoa = pessoa.montante,
            montante_empresa = pessoa.montante_empresa, contrib_empresa = pessoa.contribuicao_empresa, contrib_pessoa = pessoa.contribuicao_mensal, percentual_salario = "10%",
            fator_atuarial = pessoa.fator_atuarial())
     #return jsonify(json.loads(request.data))
    else:
        return jsonify( nome = "sem json", montante_pessoa = 100.00, montante_empresa = 50.00, contrib_empresa = 25.00, contrib_pessoa = 50.00, percentual_salario ="10%")



if __name__ == '__main__':
    app.debug = True
    app.run("0.0.0.0")
