from datetime import datetime
from dateutil.relativedelta import relativedelta


class Pessoa(object):
    '''docstring for pessoa'''
    def __init__(self, pessoa):
        super(Pessoa, self).__init__()
        #print pessoa
        self.id = pessoa['id']
        self.sexo = pessoa['sexo']
        if pessoa.has_key('nome'):
            self.nome = pessoa['nome'].strip()
        else:
            self.nome = "nao passado"
        
        if type(pessoa['data_nascimento']).__name__ == 'datetime':
            self.data_nascimento = pessoa['data_nascimento']
        else:
            self.data_nascimento = datetime.strptime(pessoa['data_nascimento'], '%Y-%m-%d')
        self.idade_aposentadoria = int(pessoa['idade_aposentadoria'])
        if str(pessoa['aporte_inicial']).strip() == '':
            self.aporte_inicial = 0.0
        else:
            self.aporte_inicial = float(pessoa['aporte_inicial'])
        if str(pessoa['aporte_empresa']).strip() == '':
            self.aporte_empresa = 0.0
        else:
            self.aporte_empresa = float(pessoa['aporte_empresa'])
        if str(pessoa['contribuicao_mensal']).strip() == '':
            self.contribuicao_mensal = 0.0
        else:
            self.contribuicao_mensal = float(pessoa['contribuicao_mensal'])
        if str(pessoa['contribuicao_empresa']).strip() == '':
            self.contribuicao_empresa = 0.0
        else:
            self.contribuicao_empresa = float(pessoa['contribuicao_empresa'])
        self.tempo_permanencia = self.tempo_aposentadoria()
        self.total_inicial = self.aporte_inicial + self.contribuicao_mensal
        self.total_inicial_empresa = self.aporte_empresa + self.contribuicao_empresa
        self.calcula()

    def data_aposentadoria(self):
        '''
        data aposentadoria
        '''
        return self.data_nascimento + relativedelta(years=self.idade_aposentadoria)

    def tempo_aposentadoria(self):
        '''
        calcula tempo para aposentadoria em meses
        '''
        meses = (self.data_aposentadoria().year - datetime.now().year) * 12 +  self.data_aposentadoria().month - datetime.now().month
        if  self.data_aposentadoria().day <  datetime.now().day:
            meses = meses - 1

        return meses

    def calcula(self):
        '''
        calcula o montante que uma pessoa tem a receber
        por default  sempre sera para calcular mensal
        '''

        montante = self.aporte_inicial 
        montante_empresa = self.aporte_empresa
        memoria = []
        total_pessoa = montante
        tota_empresa  = montante_empresa
        for i in xrange(0, self.tempo_permanencia):
            taf = self.taf(i, montante, self.contribuicao_mensal)
            rentabilidade_media = self.rentabilidade_media()
            juros_reais = self.juros_anual_para_mensal(rentabilidade_media, taf)
            montante = montante * (1 + juros_reais)
            carregamento_mensal = 1 - self.carregamento(i, montante, self.contribuicao_mensal)
            montante = montante + (self.contribuicao_mensal * carregamento_mensal)
            total_pessoa = total_pessoa + self.contribuicao_mensal
            taf = self.taf(i, montante_empresa, self.contribuicao_empresa)
            juros_reais = self.juros_anual_para_mensal(rentabilidade_media, taf)
            montante_empresa = montante_empresa * (1 + juros_reais)
            carregamento_empresa = 1 - self.carregamento(i, montante_empresa, self.contribuicao_empresa)
            montante_empresa = montante_empresa + (self.contribuicao_empresa * carregamento_empresa)
            tota_empresa = tota_empresa + self.contribuicao_empresa
            memoria.append({'mes': i, 'juros_reais': juros_reais,
                'taf': taf, 'rentabilidade_media': rentabilidade_media, 'carregamento_mensal':
                carregamento_mensal, 'carregamento_empresa': carregamento_empresa, 'contribuicao_mensal':
                self.contribuicao_mensal, 'montante': montante, 'montante_empresa': montante_empresa})
        self.montante = montante
        self.total_contrib = total_pessoa
        self.total_contrib_empresa = tota_empresa
        self.montante_empresa = montante_empresa
        self.total = montante + montante_empresa
        return memoria

    def carregamento(self, tempo, montante, valor_aporte):
        '''
        calcula carregamento baseado em fatores
        '''
        return 0.03

    def rentabilidade_media(self):

        return 0.06

    def taf(self, tempo, montante, valor_aporte):
        '''
        calcula taf baseado em fatores
        '''
        return 0.015

    def juros_anual_para_mensal(self, juros_anual, taf=0):
            '''
            conversor de juros anual em mensal
            '''

            return (((1 + juros_anual) / (1 + taf)) ** (1.0 / 12.0) - 1)

    def juros_beneficio(self):

        return 0.0

    def fator_atuarial(self):
        '''
        calcula fator atuarial
        '''
        x = self.idade_aposentadoria
        i = self.juros_beneficio()
        #ax = 1.0
        lx_anterior = 1.0
        qx_anterior = 0.0
        dx = {}
        dx[0] = 0.0
        k = 1.0 + i
        idade = 0
        nx_sum = {}
        fatores = self.tabua()
        for fator in fatores:
            lx = lx_anterior * (1 - qx_anterior)
            qx_anterior = fator
            lx_anterior = lx
            dx[idade] = lx / (k ** idade)
            for j in range(idade + 1):
                if len(nx_sum) <= j:
                     nx_sum[j] = 0.0
                nx_sum[j] = float(nx_sum[j]) + dx[idade]
            idade = idade + 1
        

        return ((1 / ((dx[x] / nx_sum[x + 1]))) + (11.0 / 24.0))

    def tabua(self):
        '''
        tabua unica no momento
        '''
        return [0.002311,
                0.000906,
                0.000504,
                0.000408,
                0.000357,
                0.000324,
                0.000301,
                0.000286,
                0.000328,
                0.000362,
                0.00039,
                0.000413,
                0.000431,
                0.000446,
                0.000458,
                0.00047,
                0.000481,
                0.000495,
                0.00051,
                0.000528,
                0.000549,
                0.000573,
                0.000599,
                0.000627,
                0.000657,
                0.000686,
                0.000714,
                0.000738,
                0.000758,
                0.000774,
                0.000784,
                0.000789,
                0.000789,
                0.00079,
                0.000791,
                0.000792,
                0.000794,
                0.000823,
                0.000872,
                0.000945,
                0.001043,
                0.001168,
                0.001322,
                0.001505,
                0.001715,
                0.001948,
                0.002198,
                0.002463,
                0.00274,
                0.003028,
                0.00333,
                0.003647,
                0.00398,
                0.004331,
                0.004698,
                0.005077,
                0.005465,
                0.005861,
                0.006265,
                0.006694,
                0.00717,
                0.007714,
                0.008348,
                0.009093,
                0.009968,
                0.010993,
                0.012188,
                0.013572,
                0.01516,
                0.016946,
                0.01892,
                0.021071,
                0.023388,
                0.025871,
                0.028552,
                0.031477,
                0.034686,
                0.038225,
                0.042132,
                0.046427,
                0.051128,
                0.05625,
                0.061809,
                0.067826,
                0.074322,
                0.081326,
                0.088863,
                0.096958,
                0.105631,
                0.114858,
                0.124612,
                0.134861,
                0.145575,
                0.156727,
                0.16829,
                0.180245,
                0.192565,
                0.205229,
                0.218683,
                0.233371,
                0.249741,
                0.268237,
                0.289305,
                0.313391,
                0.34094,
                0.372398,
                0.40821,
                0.448823,
                0.494681,
                0.546231,
                0.603917,
                0.668186,
                0.739483,
                0.818254,
                0.904945,
                1]
